import os
import requests
import csv

# GitLab API endpoint and your personal access token
GITLAB_URL = 'https://gitlab.com/api/v4/'
PRIVATE_TOKEN = os.environ.get('GITLAB_GROUP_ACCESS_TOKEN')

# Group ID to start with
ROOT_GROUP_ID = int(os.environ.get('GITLAB_GROUP_ID', 0))

# Fetches details of a specific group from GitLab.
def get_group_details(group_id):
    url = f"{GITLAB_URL}groups/{group_id}"
    headers = {"PRIVATE-TOKEN": PRIVATE_TOKEN}
    response = requests.get(url, headers=headers)
    return response.json() if response.status_code == 200 else None

# Fetches projects within a specific group from GitLab.
def get_group_projects(group_id):
    url = f"{GITLAB_URL}groups/{group_id}/projects"
    headers = {"PRIVATE-TOKEN": PRIVATE_TOKEN}
    projects = []
    page = 1
    while True:
        params = {"page": page}
        response = requests.get(url, headers=headers, params=params)
        if response.status_code == 200:
            page_projects = response.json()
            if not page_projects:
                break
            projects.extend(page_projects)
            page += 1
        else:
            print(f"Failed to get projects for group ID {group_id}")
            break
    return projects

# Recursively processes groups and their projects/subgroups.
def process_group(group_id, depth=0):
    # Retrieve group details
    group_details = get_group_details(group_id)
    if group_details is None:
        print(f"Failed to get details for group ID {group_id}")
        return

    # Print group name with indentation based on depth
    print("  " * depth + f"- {group_details['full_path']}")

    # Process projects within the group
    projects = get_group_projects(group_id)
    for project in projects:
        print("  " * (depth + 1) + f"* {project['name']}")

        with open('projects_in_gitlab_group.csv', 'a', newline='') as file:
            writer = csv.writer(file)
            if file.tell() == 0:  # Check if file is empty
                writer.writerow(["Project_ID", "Project_Name", "Project_Path"])  # Write header
            writer.writerow([project['id'], project['name'], group_details['full_path']])

    # Retrieve subgroups and recursively process them
    subgroups = get_subgroups(group_id)
    for subgroup in subgroups:
        subgroup_id = subgroup['id']
        process_group(subgroup_id, depth + 1)

# Fetches subgroups of a specific group from GitLab.
def get_subgroups(parent_group_id):
    url = f"{GITLAB_URL}groups/{parent_group_id}/subgroups"
    headers = {"PRIVATE-TOKEN": PRIVATE_TOKEN}
    subgroups = []
    page = 1
    while True:
        params = {"page": page}
        response = requests.get(url, headers=headers, params=params)
        if response.status_code == 200:
            page_subgroups = response.json()
            if not page_subgroups:
                break
            subgroups.extend(page_subgroups)
            page += 1
        else:
            print(f"Failed to get subgroups for group ID {parent_group_id}")
            break
    return subgroups

# Main function of the script.
def main():
    # Process the root group
    process_group(ROOT_GROUP_ID)

if __name__ == "__main__":
    main()
