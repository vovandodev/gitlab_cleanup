import os
import csv
import requests

# GitLab API endpoint and your personal access token
GITLAB_URL = 'https://gitlab.com/api/v4/'
PRIVATE_TOKEN = os.environ.get('GITLAB_GROUP_ACCESS_TOKEN')

# Fetches details of a specific project from GitLab.
def get_project_details(project_id):
    url = f"{GITLAB_URL}projects/{project_id}"
    headers = {"PRIVATE-TOKEN": PRIVATE_TOKEN}
    response = requests.get(url, headers=headers)
    return response.json() if response.status_code == 200 else None

# Checks the status of a project with the given project_id is it already "deleted".
def check_pending_deletion(project_details):
    if project_details and "-deleted-" in project_details['name']:
        print(f"Project '{project_details['name']}' is already scheduled for deletion. Skipping.")
        return True
    return False

# Sends a DELETE request to GitLab's API to delete a project with the given project_id.
def delete_project(project_id):
    url = f"{GITLAB_URL}projects/{project_id}"
    headers = {"PRIVATE-TOKEN": PRIVATE_TOKEN}
    response = requests.delete(url, headers=headers)
    return response.status_code

# Main function of the script.
def main():
    # Read project IDs and names from CSV and delete projects
    with open('projects_for_deleting.csv', 'r') as file:
        reader = csv.reader(file)
        next(reader)  # Skip header row
        for row in reader:
            project_id, project_name = row[0], row[1]
            project_details = get_project_details(project_id)
            if check_pending_deletion(project_details):
                continue  # Skip this project if it's already scheduled for deletion
            delete_status = delete_project(project_id)
            if delete_status == 202:
                print(f"Initiated deletion of project '{project_name}' (ID: {project_id}).")
            elif delete_status == 204:
                print(f"Project '{project_name}' (ID: {project_id}) deleted successfully.")
            else:
                print(f"Failed to delete project '{project_name}' (ID: {project_id}). Status code: {delete_status}")

if __name__ == "__main__":
    main()
